package com.bookify.exception;

import static com.bookify.constants.Constants.ERROR_CODE_GENERIC;
import static com.bookify.constants.Constants.ERROR_CODE_GENERIC_MSG;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BOOKING_DATE;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BOOKING_DATE_MSG;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bookify.model.ExceptionResponse;


@ControllerAdvice
@RestController
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{

	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> handleAllException(Exception ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_GENERIC ,ERROR_CODE_GENERIC_MSG,new Date(),ex.getMessage());
		
		return new ResponseEntity<ExceptionResponse>(obj,  HttpStatus.INTERNAL_SERVER_ERROR);
	
	}
	

	@ExceptionHandler(EmptyBookingDateException.class)
	public final ResponseEntity<ExceptionResponse> handleEmptyBookingDateException(EmptyBookingDateException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_EMPTY_BOOKING_DATE , ERROR_CODE_EMPTY_BOOKING_DATE_MSG, new Date(),ex.getMessage() );
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_ACCEPTABLE);
	}
	
	@ExceptionHandler(InvalidIndexException.class)
	public final ResponseEntity<ExceptionResponse> handleInvalidIndexException(InvalidIndexException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_EMPTY_BOOKING_DATE , ERROR_CODE_EMPTY_BOOKING_DATE_MSG, new Date(),ex.getMessage() );
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_ACCEPTABLE);
	}

}
