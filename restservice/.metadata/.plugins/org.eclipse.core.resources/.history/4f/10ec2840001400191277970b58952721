package com.bookify.dao;

import java.time.LocalDate;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bookify.model.Booking;

@Repository
public class AvailabilityDaoImpl implements AvailabilityDao {
	
	private static final Logger log = LogManager.getLogger(AvailabilityDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private BookingDao bookingDao;

	@SuppressWarnings("unchecked")
	@Override
	public List<Booking> getConflictingBookings(Booking booking) {
	 	log.debug("###### : Dao : AvailabilityDaoImpl : getConflictingBookings : start");	
		
		Session session = sessionFactory.getCurrentSession();
		String hqlQuery = "from Booking as b where (b.startDate between :sdate and :edate)"
				+ " or"
				+ " (b.endDate between :sdate and :edate)";
		
		List<Booking> bookings = session.createQuery(hqlQuery)
										.setParameter("sdate", booking.getStartDate())
										.setParameter("edate", booking.getEndDate())
										.getResultList();
		
		return bookings;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public List<Booking> getNonConflictingBookings(Booking booking) {
		log.debug("###### : Dao : AvailabilityDaoImpl : getNonConflictingBookings : start");	
		List<Booking> bookingsAfterCurrentDate = bookingDao.getNextBooking(LocalDate.now()); 
		return bookingsAfterCurrentDate;
	}


}
