package com.bookify.exception;

import static com.bookify.constants.Constants.ERROR_CODE_GENERIC;

import static com.bookify.constants.Constants.ERROR_CODE_GENERIC_MSG;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BOOKING_DATE;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BOOKING_DATE_MSG;
import static com.bookify.constants.Constants.ERROR_CODE_INVLD_INDX;
import static com.bookify.constants.Constants.ERROR_CODE_INVLD_INDX_MSG;
import static com.bookify.constants.Constants.ERROR_CODE_INVLD_ARG;
import static com.bookify.constants.Constants.ERROR_CODE_INVLD_ARG_MSG;
import static com.bookify.constants.Constants.ERROR_CODE_BKNG_CNFLCT;
import static com.bookify.constants.Constants.ERROR_CODE_BKNG_CNFLCT_MSG;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BKNG_SGGNTN;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BKNG_SGGNTN_MSG;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.bookify.model.ExceptionResponse;



@ControllerAdvice
@RestController
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{

	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> handleAllException(Exception ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_GENERIC ,ERROR_CODE_GENERIC_MSG, LocalDateTime.now(),ex.getMessage());
		
		return new ResponseEntity<ExceptionResponse>(obj,  HttpStatus.INTERNAL_SERVER_ERROR);
	
	}
	

	@ExceptionHandler(EmptyBookingDateException.class)
	public final ResponseEntity<ExceptionResponse> handleEmptyBookingDateException(EmptyBookingDateException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_EMPTY_BOOKING_DATE , ERROR_CODE_EMPTY_BOOKING_DATE_MSG, LocalDateTime.now(),ex.getMessage() );
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_ACCEPTABLE);
	}
	
	@ExceptionHandler(InvalidIndexException.class)
	public final ResponseEntity<ExceptionResponse> handleInvalidIndexException(InvalidIndexException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_INVLD_INDX , ERROR_CODE_INVLD_INDX_MSG, LocalDateTime.now(),ex.getMessage() );
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_ACCEPTABLE);
	}

	@ExceptionHandler(InvalidArgumentException.class)
	public final ResponseEntity<ExceptionResponse> handleInvalidArgumentException(InvalidArgumentException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_INVLD_ARG , ERROR_CODE_INVLD_ARG_MSG, LocalDateTime.now(),ex.getMessage() );
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_ACCEPTABLE);
	}

	@ExceptionHandler(BookingConflictException.class)
	public final ResponseEntity<ExceptionResponse> handleBookingConflictException(BookingConflictException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_BKNG_CNFLCT , ERROR_CODE_BKNG_CNFLCT_MSG, LocalDateTime.now(),ex.getMessage() );
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_ACCEPTABLE);
	}
	
	@ExceptionHandler(EmptyBookingSuggestionException.class)
	public final ResponseEntity<ExceptionResponse> handleEmptyBookingSuggestionException(EmptyBookingSuggestionException ex, WebRequest request){
		
		ExceptionResponse obj = new ExceptionResponse(ERROR_CODE_EMPTY_BKNG_SGGNTN , ERROR_CODE_EMPTY_BKNG_SGGNTN_MSG, new Date(),ex.getMessage() );
		
		return new ResponseEntity<ExceptionResponse>(obj, HttpStatus.NOT_ACCEPTABLE);
	}

}
