package com.bookify.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookify.dao.AvailabilityDao;

@Service
@Transactional
public class AvailabilityServiceImpl<T> implements AvailabilityService<T> {

	private static final Logger log = LogManager.getLogger(AvailabilityServiceImpl.class);
	
	@Autowired
	private AvailabilityDao<T> availabilityDao;
	
	@Override
	public boolean isSlotAvailable(T obj) {
		log.debug("###### : Dao : AvailabilityDao : isSlotAvailable : start");
		boolean retVal = false;
		try {
			
			List<T> bookings = availabilityDao.getConflictingBooking(obj);
			if(bookings.isEmpty()) {
				log.debug("###### : Dao : AvailabilityDao : isSlotAvailable : bookings is empty");
				retVal =  true;
			}
			
		} catch (Exception e) {
			
			log.error("###### : Dao : AvailabilityServiceImpl : isSlotAvailable : ", e);
		}
		return retVal;
	}

	@Override
	public List<T> getAvailableSlots(T booking) {
		log.debug("###### : Dao : AvailabilityDao : getAvailableSlots : start");
		List<T> bookings;
		if(isSlotAvailable(booking)) {
			bookings = new ArrayList<>();
			bookings.add(booking);
			
		}else{
			
			bookings = availabilityDao.getNonConflictingSlots(booking);
		}
		
		return booking;
	}

		
}
