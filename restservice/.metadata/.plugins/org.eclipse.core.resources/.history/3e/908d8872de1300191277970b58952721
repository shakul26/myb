package com.bookify.dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bookify.model.Booking;

@Repository
public class AvailabilityDaoImpl<T> implements AvailabilityDao<T> {
	
	private static final Logger log = LogManager.getLogger(AvailabilityDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> getConflictingBooking(T obj) {
	 
		log.debug("###### : Dao : AvailabilityDaoImpl : isSlotAvailable : start");	
		Booking booking = (Booking)obj;
		Session session = sessionFactory.getCurrentSession();
		String hqlQuery = "from Booking as b where (b.startDate between :sdate and :edate)"
				+ " or"
				+ " (b.endDate between :sdate and :edate)";
		
		List<T> bookings = session.createQuery(hqlQuery)
										.setParameter("sdate", booking.getStartDate())
										.setParameter("edate", booking.getEndDate())
										.getResultList();
		
		return bookings;
	}

}
