package com.bookify.dao;

import java.util.List;

import com.bookify.exception.EmptyBookingDateException;
import com.bookify.model.Booking;

public interface AvailabilityDao{

	public List<Booking> getConflictingBookings(Booking obj);

	public List<Booking> getNonConflictingBookings(Booking booking) throws EmptyBookingDateException;
}
