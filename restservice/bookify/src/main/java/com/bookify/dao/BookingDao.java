package com.bookify.dao;

import java.time.LocalDate;
import java.util.List;

import com.bookify.model.Booking;

public interface BookingDao {
	/**
	 * @author Shakul.Dubey
	 * This method returns the List of Booking from the given date where given date is inclusive
	 * @param date
	 * @return
	 */
	public List<Booking> getNextBooking(LocalDate date);
	
	public void addBooking(Booking booking);
}
