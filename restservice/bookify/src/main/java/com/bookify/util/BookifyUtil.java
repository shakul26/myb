package com.bookify.util;

import static com.bookify.constants.Constants.ADVANCE_BOOKING_PERIOD;
import static com.bookify.constants.Constants.INDEX_DEFAULT_NGTV;
import static com.bookify.constants.Constants.INDX_TYP_END;
import static com.bookify.constants.Constants.INDX_TYP_STRT;
import static com.bookify.constants.Constants.MSG_END_DT_NULL;
import static com.bookify.constants.Constants.MSG_STRT_DT_NULL;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.bookify.exception.EmptyBookingDateException;
import com.bookify.exception.InvalidIndexException;
import com.bookify.model.Booking;

public class BookifyUtil {

	
	public static List<Booking> getSuggestion(List<Booking> bookings, Booking booking) throws EmptyBookingDateException{
	

		String status [] = buildStatus(bookings);
		return getAvailableSlots(booking,status);
	
	}
	
	private static List<Booking> getAvailableSlots(Booking booking, String[] status) {
		
		List<Booking> availableSlots = new ArrayList<>();
		long bookingDurationRequested = ChronoUnit.DAYS.between(booking.getStartDate(), booking.getEndDate());
		
		int indexFrom = -1, indexTo = -1;
		for(int i=0; i<status.length; i++) {
			
			if(null == status[i]) {
				
				if(isIndexInitialized(indexFrom)) {
					++indexTo;
					
					if((indexTo-indexFrom +1) == bookingDurationRequested) {
						LocalDate sDate = LocalDate.now().plusDays(indexFrom); 
						LocalDate eDate = sDate.plusDays(bookingDurationRequested);
						Booking obj = new Booking(sDate, eDate);
						availableSlots.add(obj);
						
						indexFrom = indexTo+1;
					}
					
				}else {
					
					indexFrom = i;
					indexTo = i;
				}
			}
			
		}
		return availableSlots;
	}

	
	private static boolean isIndexInitialized(int index) {
		return index != INDEX_DEFAULT_NGTV;
	}
	private static String[] buildStatus(List<Booking> bookings) throws EmptyBookingDateException, InvalidIndexException{
		String status [] = new String[ADVANCE_BOOKING_PERIOD];
		
		for(Booking o : bookings) {
		
			int startIndex = getIndex(o, INDX_TYP_STRT);
			if(startIndex < 0 && startIndex>= ADVANCE_BOOKING_PERIOD)
				throw new InvalidIndexException("Index out of range");
			
			int endIndex =  getIndex(o, INDX_TYP_END);
			for(int i = startIndex; i<=endIndex; i++) {
				status[i] = o.getStatus();	
			}
			
		}
		return status;
	}
	
	private static int getIndex(Booking booking, String indexType) throws EmptyBookingDateException{
		
		LocalDate currentDate = LocalDate.now();
		int dayOfMonth = currentDate.getDayOfMonth();
		
		if(INDX_TYP_STRT.equalsIgnoreCase(indexType)) {

			if(null == booking.getStartDate())
				throw new EmptyBookingDateException(MSG_STRT_DT_NULL);
			
			return booking.getStartDate().getDayOfMonth() % dayOfMonth;
					
		}else if(INDX_TYP_END.equalsIgnoreCase(indexType)){
			if(null == booking.getEndDate())
				throw new EmptyBookingDateException(MSG_END_DT_NULL);
			
			return booking.getEndDate().getDayOfMonth() % dayOfMonth;
			
		}
		
		return -1;
	}
	/*public static void main(String[] args) {
		LocalDate currentDate = LocalDate.now();
		int dayOfMonth = currentDate.getDayOfMonth();
		System.out.println(dayOfMonth);
	}*/
}
