package com.bookify.controller;

import static com.bookify.constants.Constants.URL_AVLBL;
import static com.bookify.constants.Constants.URL_BOOKIFY;
import static com.bookify.constants.Constants.URL_BOOKING;
import static com.bookify.constants.Constants.URL_V1;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.bookify.dao.BookingDao;
import com.bookify.model.Booking;
import com.bookify.service.AvailabilityService;
import com.bookify.service.BookingService;

import static com.bookify.constants.Constants.ERROR_CODE_BKNG_CNFLCT;
import static com.bookify.constants.Constants.ERROR_CODE_BKNG_CNFLCT_MSG;
import static com.bookify.constants.Constants.MSG_BKNG_CRTD;
@RestController
@RequestMapping(URL_BOOKIFY+URL_V1)
public class BookingController {
	private static final Logger log = LogManager.getLogger(BookingController.class);
	
	@Autowired
	private BookingService bookingService;
	
	@Autowired
	private AvailabilityService availabilityService;
	
	@PostMapping(URL_BOOKING)
	public ResponseEntity<?> addBooking(@RequestBody Booking booking, UriComponentsBuilder ucBuilder){
		log.debug("###### : Controller : BookingController : addBooking : start");
		
		if(!availabilityService.isSlotAvailable(booking)) {
			log.warn(ERROR_CODE_BKNG_CNFLCT+" : "+ERROR_CODE_BKNG_CNFLCT_MSG);
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		
		bookingService.addBooking(booking);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/booking/{bookingId}").buildAndExpand(booking.getBookingId()).toUri());
		return new ResponseEntity<String>(MSG_BKNG_CRTD,headers, HttpStatus.CREATED);
	}
	
}
