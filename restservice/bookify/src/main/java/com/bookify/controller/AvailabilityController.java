package com.bookify.controller;

import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BKNG_SGGNTN;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BKNG_SGGNTN_MSG;
import static com.bookify.constants.Constants.URL_AVLBL;
import static com.bookify.constants.Constants.URL_BOOKIFY;
import static com.bookify.constants.Constants.URL_BOOKING;
import static com.bookify.constants.Constants.URL_V1;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bookify.model.Booking;
import com.bookify.model.ExceptionResponse;
import com.bookify.service.AvailabilityService;

@RestController
@RequestMapping(URL_BOOKIFY+URL_V1)
public class AvailabilityController {

	private static final Logger log = LogManager.getLogger(AvailabilityController.class);
	
	@Autowired
	private AvailabilityService availabilityService;
	
	@PostMapping(URL_BOOKING+URL_AVLBL)
	public ResponseEntity<?> verifyBookingAvailability(@RequestBody Booking booking){
		log.debug("###### : Controller : AvailabilityController : verifyBookingAvailability : start");
		List<Booking> availableBookings =  availabilityService.getAvailableSlots(booking);
		
		if(availableBookings.isEmpty()) {
			return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(ERROR_CODE_EMPTY_BKNG_SGGNTN, ERROR_CODE_EMPTY_BKNG_SGGNTN_MSG, LocalDateTime.now()), HttpStatus.OK);
		}
		return new ResponseEntity<List<Booking>>(availableBookings, HttpStatus.OK);
	}
}
