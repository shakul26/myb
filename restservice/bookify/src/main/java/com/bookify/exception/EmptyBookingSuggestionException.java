package com.bookify.exception;

public class EmptyBookingSuggestionException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7634371234289951927L;

	public EmptyBookingSuggestionException(String msg) {
		
		super(msg);
	}

}
