package com.bookify.exception;

public class EmptyBookingDateException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1018781064122206190L;

	public EmptyBookingDateException(String msg) {
		super(msg);
	}
}
