package com.bookify.exception;

public class BookingConflictException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -221087740629888466L;

	public BookingConflictException(String msg) {
		super(msg);
	}
}
