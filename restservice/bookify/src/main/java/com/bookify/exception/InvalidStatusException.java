package com.bookify.exception;

public class InvalidStatusException extends RuntimeException{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidStatusException(String msg) {
		super(msg);
	}
}
