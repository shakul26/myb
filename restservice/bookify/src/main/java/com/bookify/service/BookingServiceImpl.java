package com.bookify.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static com.bookify.constants.Constants.ERROR_CODE_INVLD_ARG;

import javax.transaction.Transactional;

import com.bookify.dao.BookingDao;
import com.bookify.exception.InvalidArgumentException;
import com.bookify.model.Booking;

@Service  
@Transactional
public class BookingServiceImpl implements BookingService{

	private final static Logger log = LogManager.getLogger(BookingServiceImpl.class);

	@Autowired
	private BookingDao bookingDao;
	
	@Override
	public boolean addBooking(Booking booking) {
		
		log.debug("###### : Service : BookingServiceImpl : addBooking : start");
		boolean retVal = false;
		
		try {
			
			if(null == booking)
				throw new InvalidArgumentException("Booking object is null");
			bookingDao.addBooking(booking);
			retVal = true;
			
		} catch (InvalidArgumentException e) {
			log.error(ERROR_CODE_INVLD_ARG,e);
			
		}catch (Exception e) {
			log.error(e);
		}
		return retVal;
	}
	
	
}
