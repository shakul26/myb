package com.bookify.service;

import java.util.List;

import com.bookify.model.Booking;

public interface AvailabilityService{

	public boolean isSlotAvailable(Booking booking);

	public List<Booking> getAvailableSlots(Booking booking);
}
