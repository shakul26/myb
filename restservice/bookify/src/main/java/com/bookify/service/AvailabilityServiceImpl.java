package com.bookify.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bookify.dao.AvailabilityDao;
import com.bookify.exception.EmptyBookingDateException;
import com.bookify.exception.EmptyBookingSuggestionException;
import com.bookify.model.Booking;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BKNG_SGGNTN;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BKNG_SGGNTN_MSG;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BKNG_DATE;
import static com.bookify.constants.Constants.ERROR_CODE_EMPTY_BKNG_DATE_MSG;

@Service
@Transactional
public class AvailabilityServiceImpl implements AvailabilityService {

	private static final Logger log = LogManager.getLogger(AvailabilityServiceImpl.class);
	
	@Autowired
	private AvailabilityDao availabilityDao;
	
	@Override
	public boolean isSlotAvailable(Booking booking) {
		log.debug("###### : Dao : AvailabilityDao : isSlotAvailable : start");
		boolean retVal = false;
		try {
			
			List<Booking> bookings = availabilityDao.getConflictingBookings(booking);
			if(bookings.isEmpty()) {
				log.debug("###### : Dao : AvailabilityServiceImpl : isSlotAvailable : bookings is empty");
				retVal =  true;
			}
			
		} catch (Exception e) {
			
			log.error("###### : Dao : AvailabilityServiceImpl : isSlotAvailable : ", e);
		}
		
		log.debug("###### : Dao : AvailabilityServiceImpl : isSlotAvailable : end "+ retVal);
		return retVal;
	}

	@Override
	public List<Booking> getAvailableSlots(Booking booking) {
		log.debug("###### : Dao : AvailabilityServiceImpl : getAvailableSlots : start");
		List<Booking> bookings = null;
		
		try {
		if(isSlotAvailable(booking)) {
			bookings = new ArrayList<>();
			bookings.add(booking);
			log.info("###### : getAvailableSlots : Requested Booking is Available"+booking.toString());
		}else{
			
			bookings = availabilityDao.getNonConflictingBookings(booking);
			log.info("###### : getAvailableSlots : Requested Booking is not Available, suggested slots are "+ bookings);
			
			if(bookings.isEmpty()) {
				throw new EmptyBookingSuggestionException(ERROR_CODE_EMPTY_BKNG_SGGNTN);
			}
		
		}
		
		}catch (EmptyBookingDateException e) {
			log.error(ERROR_CODE_EMPTY_BKNG_DATE+" : "+ ERROR_CODE_EMPTY_BKNG_DATE_MSG);
		}
		catch (EmptyBookingSuggestionException e) {
			log.error(ERROR_CODE_EMPTY_BKNG_SGGNTN+" : "+ ERROR_CODE_EMPTY_BKNG_SGGNTN_MSG);
			
		}catch (Exception e) {
			log.error(e);
		}
		
		return bookings;
	}

		
}
