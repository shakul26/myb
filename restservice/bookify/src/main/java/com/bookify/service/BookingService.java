package com.bookify.service;

import com.bookify.model.Booking;

public interface BookingService {

	public boolean addBooking(Booking booking);
}
