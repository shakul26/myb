package com.bookify.constants;


/**
 * This is the enumerated list of booking statuses that shows the stage of booked event to proceed further.
 * 
 * @author Shash
 * @version 1.0
 */
public enum Status {
	BOOKED("booked"),
	CONFIRMED("confirmed"),
	INPROGRESS("in_progress"),
	PAYMENT_PENDING("payment_pending"),
	COMPLETED("completed");
	
	private final String status;
	
	/**
	 * The constructor which sets the actual value of the status.
	 */
	Status(String status){
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
	
	@Override
	public String toString() {

		return status;
	}

}
