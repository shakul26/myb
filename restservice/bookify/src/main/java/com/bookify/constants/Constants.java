package com.bookify.constants;

/**
 * 
 * @author Shakul.Dubey
 *
 * Class contains all the constants
 */
public class Constants {

	public static final String URL_BOOKIFY = "/myb"; // make your booking
	public static final String URL_V1 = "/v1";
	public static final String URL_BOOKING = "/booking";
	public static final String URL_AVLBL = "/available";
	public static final String URL_PREV = "/prev";

	/**
	 * ERROR_CODE
	 */
	public static final String ERROR_CODE_GENERIC = "MYB_APP_000";
	public static final String ERROR_CODE_GENERIC_MSG = "Generic Error, Something went Wrong!!";
	public static final String ERROR_CODE_EMPTY_BKNG_DATE = "MYB_APP_001";
	public static final String ERROR_CODE_EMPTY_BKNG_DATE_MSG = "Date is Empty in Booking Object!!";
	public static final String ERROR_CODE_INVLD_INDX = "MYB_APP_005";
	public static final String ERROR_CODE_INVLD_INDX_MSG = "Inavlid index found!!";
	public static final String ERROR_CODE_INVLD_ARG = "MYB_APP_010";
	public static final String ERROR_CODE_INVLD_ARG_MSG = "Inavlid Argument!!";
	public static final String ERROR_CODE_BKNG_CNFLCT = "MYB_APP_015";
	public static final String ERROR_CODE_BKNG_CNFLCT_MSG = "Booking is Conflicting!!";
	public static final String ERROR_CODE_EMPTY_BKNG_SGGNTN = "MYB_APP_020";
	public static final String ERROR_CODE_EMPTY_BKNG_SGGNTN_MSG = "Suggestion List is Empty!!";
	
	
	
	
	
	/**
	 * FIELD CONSTANT
	 */
	public static final int ADVANCE_BOOKING_PERIOD = 90; //number of days
	public static final String INDX_TYP_STRT = "start";
	public static final String INDX_TYP_END = "end";
	public static final int INDEX_DEFAULT_NGTV = -1;
	
	
	/**
	 * MSG CONSTANT
	 */
	public static final String MSG_STRT_DT_NULL = "Start Date is null";
	public static final String MSG_END_DT_NULL = "Start Date is null";
	public static final String MSG_BKNG_CRTD = "Booking Created";
	


}
