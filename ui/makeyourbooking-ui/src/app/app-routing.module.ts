import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListBookingsComponent } from './components/booking/list-bookings.component';
import { CreateBookingComponent } from './components/booking/create-booking.component';
import { CalendarComponent } from './components/calendar/component';
import { AvailabilityComponent } from './components/availability/availability.component';
import { from } from 'rxjs';
const appRoutes: Routes = [
  { path: 'myb/list', component: ListBookingsComponent},
  { path: 'myb/create', component: CreateBookingComponent},
  { path: 'myb/available', component: AvailabilityComponent},
  
  { path: '', redirectTo: '/myb/available', pathMatch:'full'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
