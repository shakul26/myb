export class User{

    userId: String;
    firstName: String;
    lastName: String;
    email: String;
}