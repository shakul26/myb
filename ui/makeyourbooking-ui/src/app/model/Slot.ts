export class Slot{

    startDate: Date;
    endDate: Date;

    constructor(startDate: Date, endDate: Date){
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public  getStartDate() : Date{
        return this.startDate
    }

    public  getEndDate() : Date{
        return this.endDate
    }
}