import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from "@angular/router";
import { AvailabilityService } from "../../service/availability.service";
import { Slot } from '../../model/Slot';
@Component({
  selector: 'app-availability',
  templateUrl: './availability.component.html',
  styleUrls: ['./availability.component.css']
})
export class AvailabilityComponent implements OnInit {

  availabilityForm : FormGroup;
  submitted = false;
  slots: Slot[];
  startDate : Date;
  endDate : Date;
  constructor(private formBuilder: FormBuilder, private availService: AvailabilityService) {
    
   }

  ngOnInit() {
    this.availabilityForm = this.formBuilder.group({
     
      startDate : ['', Validators.required],
      endDate : ['', Validators.required]
    });
  }

  onSubmit(){
    this.submitted = true;
  
    if(this.availabilityForm.invalid)
    {
      return;
    }

    this.startDate  = this.availabilityForm.get('startDate').value;
    this.endDate  = this.availabilityForm.get('endDate').value;
    let slot = new Slot(this.startDate, this.endDate);
    
    this.availService.postAvailability(slot).then(
      slots => {
        this.slots = slots;
      },
      err =>{
        console.log(err);
      }
    );
  } 

  
}
