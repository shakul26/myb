import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/User';
import { Observable, of } from 'rxjs';
import { Slot } from '../model/Slot';
import { promise } from 'protractor';
import { HttpHeaders } from '@angular/common/http';
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from '@angular/http';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AvailabilityService {
  
  private apiUrl = 'http://localhost:9090/myb/v1';
  constructor(private http: HttpClient) { }

  

  postAvailability(slot: Slot): Promise<Array<Slot>>{
    

    return this.http.post(`${this.apiUrl}/booking/available`,JSON.stringify(slot), httpOptions)
               .toPromise()
               .then(response => response as Slot[])
               .catch(this.handleError);
  }


  private handleError(error: any): Promise<Array<any>> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
    }
}
