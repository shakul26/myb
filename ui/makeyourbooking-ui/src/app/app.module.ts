import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoModule } from './module';
import { AppComponent } from './app.component';
import { CreateBookingComponent } from './components/booking/create-booking.component';
import { ListBookingsComponent } from './components/booking/list-bookings.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { from } from 'rxjs';
import { CalendarComponent } from './components/calendar/component';
import { AvailabilityComponent } from './components/availability/availability.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CreateBookingComponent,
    ListBookingsComponent,
    HeaderComponent,
    FooterComponent,
    AvailabilityComponent 
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DemoModule,
    BsDatepickerModule.forRoot()
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
